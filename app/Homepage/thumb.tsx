'use client'
import React, { Component } from 'react'; 
import "react-responsive-carousel/lib/styles/carousel.min.css"; 
import { Carousel } from 'react-responsive-carousel'; 

export default class NextJsCarousel extends Component { 
	render() { 
		return ( 
			<div id="thumb"> 
			<Carousel> 
				<div> 
					<img src="https://picsum.photos/250/250?v=1" alt="image1" width={250} height={250}/> 
					<p className="legend">Image 1</p> 

				</div> 
				<div> 
                <img src="https://picsum.photos/250/250?v=2" alt="image2" width={250} height={250}/> 
					<p className="legend">Image 2</p> 

				</div> 
				<div> 
                <img src="https://picsum.photos/250/250?v=3" alt="image3" width={250} height={250}/> 
					<p className="legend">Image 3</p> 

				</div> 
				<div> 
                <img src="https://picsum.photos/250/250?v=4" alt="image4" width={250} height={250}/> 
					<p className="legend">Image 4</p> 

				</div> 
				<div> 
                <img src="https://picsum.photos/250/250?v=5" alt="image5" width={250} height={250}/> 
					<p className="legend">Image 5</p> 

				</div> 
			</Carousel> 
			</div> 
		); 
	} 
};
