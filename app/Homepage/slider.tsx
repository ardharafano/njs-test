'use client'
import React from 'react'
import Link from 'next/link';
import Image from 'next/image';

// splide
import { Splide, SplideTrack, SplideSlide } from '@splidejs/react-splide';
import '@splidejs/react-splide/css';
// end splide

// font
import { Pacifico } from 'next/font/google'
const pacifico = Pacifico({ subsets: ['latin'], weight: ['400'] })
// end font

const slider = () => {
  return (
    <div>
        <div className={pacifico.className}>
        <div className='w-full max-w-[970px] mx-auto my-[50px]'>
            <h2 className='mb-[20px]'>Ini Bagian Slider</h2>
            <Splide aria-label="My Favorite Images" id="slide1"
                  options={ {
                    rewind      : true,
                    autoplay    : true
                  } }>
                <SplideSlide>
                    <Link href="https://id.soccerway.com/" target="_blank">
                        <Image src="https://placehold.co/970x300/png" alt="Image 1" width={970} height={300}
                        className='w-full block max-w-[970px] mx-auto min-h-[300px] object-cover' />
                        <h2>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</h2>
                    </Link>
                </SplideSlide>
                <SplideSlide>
                    <a href="soccerway.com" target="_blank">
                        <img src="https://picsum.photos/970/300" alt="Image 1" width={970} height={300}
                        className='w-full block max-w-[970px] mx-auto min-h-[300px] object-cover'/>
                        <h2>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</h2>
                    </a>
                </SplideSlide>
            </Splide>
        </div>
        </div>

        <div className='w-full max-w-[1280] mx-auto my-[50px]'>
            <Splide aria-label="My Favorite Images" id='slide2'
                  options={ {
                    rewind      : true,
                    autoplay    : true,
                    perPage     : 2,
                    gap         : "1rem",
                    breakpoints: {
                        768: {
                            perPage: 1,
                        },
                    }
                  } }>
                <SplideSlide>
                    <img src="https://picsum.photos/1280/500" alt="Image 1" width={970} height={500}
                    className='w-full block max-w-[1280px] mx-auto min-h-[500px] object-cover'/>
                </SplideSlide>
                <SplideSlide>
                <img src="https://picsum.photos/1280/500" alt="Image 1" width={970} height={500}
                    className='w-full block max-w-[1280px] mx-auto min-h-[500px] object-cover'/>
                </SplideSlide>
                <SplideSlide>
                    <img src="https://picsum.photos/1280/500" alt="Image 1" width={970} height={500}
                    className='w-full block max-w-[1280px] mx-auto min-h-[500px] object-cover'/>
                </SplideSlide>
                <SplideSlide>
                <img src="https://picsum.photos/1280/500" alt="Image 1" width={970} height={500}
                    className='w-full block max-w-[1280px] mx-auto min-h-[500px] object-cover'/>
                </SplideSlide>
            </Splide>
        </div>
    </div>
  )
}

export default slider