import Image from 'next/image'
import Column from './Homepage/column'
import Slider from './Homepage/slider'
import Thumb from './Homepage/thumb'

export default function Home() {
  return (
    <div>
      <h2>Halo Dunia</h2>
      <Column />
      <Slider />
      <Thumb />
    </div>
  )
}
